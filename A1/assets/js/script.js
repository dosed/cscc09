$(document).ready(function() {
    loadJSON('tweets');
});

$("#clear-btn").click(function() {
    $("#tweets_list").hide("blind", 500, function() {
        $("#tweets_list").html('');
    });    
});

$("#tweets, #users, #links").click(function() {
    $("#tweets_list").hide("blind", 500, function() {
        $("#tweets_list").html('');
    });
    loadJSON($(this).attr('id'));
});

function loadJSON(query) {
    var routeURL = document.URL + query;                    
    $.ajax({
        type: 'GET',
        url: routeURL,
        dataType: "json",
        success: function(json_data) {
            if (query == "users") showUsers(json_data);
            else if (query == "tweets") showTweets(json_data);
            else if (query == "links") showLinks(json_data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function showUsers(json_data) {
    $.each(json_data, function(i, item) {
        $("#tweets_list").append("<hr><table><tr><td style='padding-right: 10px;''>"
            + "<img src='"+ item.profile_image_url +"'/></td>"
            + "<td style='padding-top: 5px;'>"
            + "<div class='span2'>@"+item.screen_name 
            + "<div class='span4'><b>"+item.name+"</b></div>"
            + "</td></tr></table>");
    });
    $("#tweets_list").show("blind", 500);
}

function showTweets(json_data) {
    $.each(json_data, function(i, item) {
        $("#tweets_list").append("<hr><table><tr><td style='padding-right: 10px;''>"
            + "<img src='"+ item.profile_image_url +"'/></td>"
            + "<td style='padding-top: 5px;'>"
            + "<div class='span4'><b>"+item.name+"\t - </b>" 
            + item.text 
            + "<span class='created_at'>" 
            + item.created_at
            + " via " + item.source
            + "</span></div></td></tr></table>");
    });                    
    $("#tweets_list").show("blind", 500);
}

function showLinks(json_data) {
    $.each(json_data, function(i, item) {
        $("#tweets_list").append("<hr><table><tr><td style='padding-right: 10px;''>"
            + "<img src='"+ item.profile_image_url +"'/></td>"
            + "<td style='padding-top: 5px;'>"
            + "<div class='span2'>@"+item.screen_name 
            + "<div class='span4'><b>"+item.name+"</b></div>"
            + item.links + "</td></tr></table>");
    });
    $("#tweets_list").show("blind", 500);
}

