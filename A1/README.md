MumbleBee
=========
Twitter clone based on creating a RESTful API for CSCC09H3 - Web Programming

Uses Node.js for backend, jQuery + jQueryUI + Twitter Bootstrap for frontend

Group Members:
==============
Kevin Hon Man Chan (chanke25, 997385752)

Catalin Tomsa ()

David Cheng ()

Donald Seo ()

Sample HTTP Requests:
=====================
Get all tweets in archive : URL:PORT/tweets

Get specific tweet        : URL:PORT/tweets/id

Get list of all users     : URL:PORT/users

Get user's tweets         : URL:PORT/users/screen_name

Get list of links         : URL:PORT/links

Get links by user         : URL:PORT/links/screen_name



