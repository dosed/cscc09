
var route = function(query, response, JSONobj) {
    var JSONresp;
    if (query.length == 1) { // return list of all users [/users]
        JSONresp = get_users(JSONobj);
    }
    else if (query.length == 2) { // return user's tweets by screen name [/users/screen_name]
        JSONresp = get_user(query[1], JSONobj);
    }
    
    response.setHeader("Content-Type", "application/json");
    response.statusCode = 200;
    response.end(JSON.stringify(JSONresp));
}

function inArray(obj, arr) {
    for (var i=0; i < arr.length; i++) {
        if (arr[i].id === obj) return true;
    }
    return false;
}

//get list of users in favs.json
function get_users(JSONobj){
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        var t = {
            'id': JSONobj[i].id_str,
            'profile_image_url': JSONobj[i].user.profile_image_url,
            'name': JSONobj[i].user.name,
            'screen_name': JSONobj[i].user.screen_name,
            'text': JSONobj[i].text,
            'created_at': JSONobj[i].created_at,
            'source': JSONobj[i].source
        }
        if(!inArray(t.id, obj)) {
            obj.push(t);
        }
    }
    return obj;
}

//get specific user based on screen_name
function get_user(screen_name, JSONobj){
    var obj = [];
    for (var i = 0, len = JSONobj.length; i < len; ++i) {
        if (screen_name == JSONobj[i].user.screen_name){
            var t = {
                'id': JSONobj[i].id_str,
                'profile_image_url': JSONobj[i].user.profile_image_url,
                'name': JSONobj[i].user.name,
                'screen_name': JSONobj[i].user.screen_name,
                'text': JSONobj[i].text,
                'created_at': JSONobj[i].created_at,
                'source': JSONobj[i].source
            }
            obj.push(t);
        }
    }
    return obj;
}

module.exports.route = route;