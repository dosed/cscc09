
var route = function(query, response, JSONobj) {
    var JSONresp;
    if (query.length == 1) { // return all tweets [/tweets]
        JSONresp = get_links(JSONobj);
    }
    else if (query.length == 2) { // return specific tweet by ID [/tweets/ID]
        JSONresp = get_link(query[1], JSONobj);
    }
    
    response.setHeader("Content-Type", "application/json");
    response.statusCode = 200;
    response.end(JSON.stringify(JSONresp));
}

//get list of links in favs.json
function get_links(JSONobj){
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        var urlArr = [];
        var urls = JSONobj[i].entities.urls;
        for (var j=0; j < urls.length; j++) {
            urlArr.push("<a href='"+urls[j].expanded_url+"'>"+urls[j].display_url+"</a>");
        }
        var t = {
            'id': JSONobj[i].id_str,
            'profile_image_url': JSONobj[i].user.profile_image_url,
            'name': JSONobj[i].user.name,
            'screen_name': JSONobj[i].user.screen_name,
            'links': urlArr.join(', ')
        }
        obj.push(t);
    }
    return obj;
}

//get links by user
function get_link(screen_name, JSONobj){
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        if (screen_name == JSONobj[i].user.screen_name){
            var urlArr = [];
            var urls = JSONobj[i].entities.urls;
            for (var j=0; j < urls.length; j++) {
                urlArr.push("<a href='"+urls[j].expanded_url+"'>"+urls[j].display_url+"</a>");
            }
            var t = {
                'id': JSONobj[i].id_str,
                'profile_image_url': JSONobj[i].user.profile_image_url,
                'name': JSONobj[i].user.name,
                'screen_name': JSONobj[i].user.screen_name,
                'links': urlArr.join(', ')
            }
            obj.push(t);
        }
    }
    return obj;
}

module.exports.route = route;