/**
 * CSCC09H3 - Assignment 1 - My favourite Things
 * Node.js RESTful API | Twitter Clone - MumbleBee
 * Kevin Chan - chanke25
 * Catalin Tomsa - 
 * David Cheng - 
 * Donald Seo - 
 */

var server = process.env.IP,
	port = process.env.PORT,
	http = require('http'),
	path = require('path'),
	url = require('url'),
	fs = require('fs'),
	validTypes = {
		".html" : "text/html",
		".json" : "application/json",
		".js"	: "application/javascript", 
		".ico"	: "image/x-icon",
		".css"	: "text/css",
		".txt"	: "text/plain",
		".jpg"	: "image/jpeg",
		".gif"	: "image/gif",
		".png"	: "image/png",
	};
	
var JSONobj;
var links = require('./controllers/links_controller.js');
var tweets = require('./controllers/tweets_controller.js');
var users = require('./controllers/users_controller.js');

console.log('Server running at ' + server + port + '/');

http.createServer(function(request, response) {
	//Load JSON into variable
	fs.readFile(__dirname + "/assets/js/favs.json", 'utf8', function(err, data){
        if(err){
            console.log('Error: ' + err);
            return;
        }
        else if (!data){
            console.log('File favs.json not found!');
            return;
        }
        else{
            JSONobj = JSON.parse(data);
        }
	});

	var pathname = url.parse(request.url).pathname;
	var filename = request.url;
	var query = request.url.split("/");
	query = query.slice(1, query.length);
	
	if (pathname === "/") {
        filename = "/index.html";
        loadFile(filename, response);
	}
	else if (query[0] === "tweets") {
        tweets.route(query, response, JSONobj);
	}
	else if (query[0] === "users") {
        users.route(query, response, JSONobj);
	}
	else if (query[0] === "links") {
        links.route(query, response, JSONobj);
	}
	else {
        filename = request.url;
        loadFile(filename, response);
	}

}).listen(port);

function loadFile(filename, response) {
	var ext = path.extname(filename);
	var currFile = __dirname + filename;
	if (validTypes[ext]) {
		fs.exists(currFile, function(exists) {
			if(exists) {
				console.log("Loading file: " + currFile);
				fs.readFile(currFile, function(err, contents) {
                    if(!err) {			
                        response.setHeader("Content-Length", contents.length);
                        response.setHeader("Content-Type", ext);
                        response.statusCode = 200;
                        response.end(contents);
                    } else {
                        response.writeHead(500);
                        response.end();
                    }
                });
			} else {
				console.log("File not found: " + currFile);
				response.writeHead(404);
				response.end();
			}
		}); 
	}
	else {
		response.writeHead(404);
		response.end('These aren\'t the droids you\'re looking for...');
	}	
}

